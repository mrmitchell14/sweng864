DROP TABLE public.recipes;
DROP TABLE public.users;
CREATE TABLE public.recipes
(
    id serial PRIMARY KEY,
    name varchar(60),
    main_ingredient varchar(128),
    sup_ingredients varchar(128),
    directions varchar(128)
);

CREATE TABLE public.users
(
    id serial NOT NULL,
    name varchar(60),
    email varchar(128) PRIMARY KEY NOT NULL,
    mobile varchar(128),
    password varchar(128),
    recipe_id integer,
    grocery_list varchar(128)
);

-- INSERT INTO users(id, email, mobile, name, password)
-- VALUES (1,'luke@email.com', '123456789', 'Luke', '1sdkjnjwef');
-- INSERT INTO users(id, email, mobile, name, password)
-- VALUES (2,'ryan@email.com', '123456789', 'Ryan', 'password123');
-- INSERT INTO users(id, email, mobile, name, password)
-- VALUES (3,'colin@email.com', '444333567', 'Colin', 'password123');
-- INSERT INTO users(id, email, mobile, name, password)
-- VALUES (4,'heather@email.com', '123456789', 'Heather', 'password123');
-- INSERT INTO users(id, email, mobile, name, password)
-- VALUES (5,'halle@email.com', '123456789', 'Halle', 'password123');

-- INSERT INTO users(id, email, mobile, name, password)
-- VALUES (6,'daisy@email.com', '123456789', 'Daisy', 'daisy');

INSERT INTO recipes(id, name, main_ingredient, sup_ingredients, directions)
VALUES (1,'Spaghetti', 'noodles', 'tomato sauce, oil, parmesan cheese', 'boil water, add noodles, cook until noodles are soft, drain water, add sauce');

INSERT INTO recipes(id, name, main_ingredient, sup_ingredients, directions)
VALUES (2,'Bean & Cheese Burritos', 'beans', 'cheese, salsa, tortillas', 'cook beans, warm tortialls, add chesse and salsa');

INSERT INTO recipes(id, name, main_ingredient, sup_ingredients, directions)
VALUES (3,'Grilled Chicken', 'chicken', 'chicken breast, seasoning, oil, salad', 'preheat bbq, cook chicken until 300 degrees on inside, prepare salad');

INSERT INTO recipes(id, name, main_ingredient, sup_ingredients, directions)
VALUES(5,'grilled hamburgers','ground beef','salt and pepper','Grill over high heat covered for 4 to 6 min per side. Add cheese. Serve with ketchup, mustard, mayo');

INSERT INTO recipes(id, name, main_ingredient, sup_ingredients, directions)
VALUES(6,'Quesadillas','cheese','tortialls, cheese, salsa','Place cheese in tortilla, cook on pan or grill until cheese is melted and tortilla is crispy' );

INSERT INTO recipes(id, name, main_ingredient, sup_ingredients, directions)
VALUES(7,'Beef Tacos','ground beef','tortillas, cheese, lettuce, salsa','Cook ground beef in pan, stir in seasoning, add cheese, lettuce, salsa to tortilla');

INSERT INTO recipes(id, name, main_ingredient, sup_ingredients, directions)
VALUES(8,'Fish Tacos','Fish','tortillas, cheese, lettuce, salsa','Cook fish in pan, stir in seasoning, add cheese, lettuce, salsa to tortilla');

INSERT INTO recipes(id, name, main_ingredient, sup_ingredients, directions)
VALUES(9,'BBQ Chicken','Chicken','BBQ sauce, potatoes, salad','Heat up grill, cover chicken with bbq sauce, cook until chicken reaches 160 degrees, serve with salada or potatoes');

INSERT INTO recipes(id, name, main_ingredient, sup_ingredients, directions)
VALUES(10,'Fried Chicken','Chicken','mash potatoes, corn, salad','Fry chicken, mash potoates and season with salt and pepper, boil corn until soft');