package com.sweng894.whats4dinner.controller;

import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.sweng894.whats4dinner.entity.Recipe;
import com.sweng894.whats4dinner.entity.User;
import com.sweng894.whats4dinner.repository.RecipeRepository;
import com.sweng894.whats4dinner.repository.UserRepository;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Message;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = {"/recipes"})
public class RecipeController {

    public static final String ACCOUNT_SID = "ACf0524579b4c540ae754bac0f4ea9b9b9";
    public static final String AUTH_TOKEN = "2f58161c61112ea9a4a62b04408a6317";
    public static final String TWILIO_NUMBER = "+14433802582";

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("home")
	public String recipes (Model model) {
		//model.addAttribute("recipes", this.recipeRepository.findAll());

		return "recipeHome";
	}
/** 
	@GetMapping("showRecipe")
    public String findRecipeByName(Model model, @RequestParam String mainIngredient) {

        Recipe recipe= (Recipe) this.recipeRepository.search(mainIngredient);

        model.addAttribute("recipe", recipe);
		System.out.println(mainIngredient);
		System.out.println(model.getAttribute(recipe.getName()));

        return "showRecipes";
    }
*/
	@RequestMapping("/search")
    public String viewSearchRecipe(Model model, @Param("mainIngredient") String mainIngredient) {
        List<Recipe> recipes = recipeRepository.findByMainIngredient(mainIngredient);

        	model.addAttribute("recipes", recipes);
        	model.addAttribute("mainIngredient", mainIngredient);
        	
            if(recipes.isEmpty()) {
                Boolean empty = true;
                model.addAttribute("empty", empty);
                model.addAttribute("mainIngredient", mainIngredient);
            }
            return "recipeHome";
    }

    @RequestMapping("/save")
    public String saveRecipe(@Param("recipe_id") Integer recipe_id, @Param("email") String email) {
            this.recipeRepository.saveRecipe(recipe_id, email);
        	return "recipeHome";

    }

    @RequestMapping("/saveToList")
    public String saveToGroceryList(Model model, @Param("ingredient") String ingredient, @Param("email") String email) {
        //List<String> supIngredients = Arrays.asList(sup_ingredients.split(",", -1));
        String currentList = this.recipeRepository.getCurrentList(email);
        String newList = "";
        if(currentList == null) {
            newList = ingredient;
        } else {
            newList = currentList + "," + ingredient;
        }
        this.recipeRepository.saveToList(newList, email);
        currentList = this.recipeRepository.getCurrentList(email);
        List<String> groceryList = Arrays.asList(currentList.split(",", -1));
        model.addAttribute("groceryList", groceryList);
        //this.recipeRepository.saveToList(supIngredients, email);
        return "createList";
    }

    @GetMapping("/createList")
	public String groceryList(Model model, @Param("sup_ingredients") String sup_ingredients, @Param("email") String email){
       
        String[] supIngredients = sup_ingredients.split(",");
        model.addAttribute("supIngredients", supIngredients);
        String currentList = this.recipeRepository.getCurrentList(email);
        if(currentList != null) {            
            List<String> groceryList = Arrays.asList(currentList.split(",", -1));
            model.addAttribute("groceryList", groceryList);
        }
        
		return "createList";
	}

    @GetMapping("/viewList")
	public String viewList(Model model,@Param("email") String email){
        String currentList = this.recipeRepository.getCurrentList(email);
        model.addAttribute("groceryList", currentList);
		return "createList";
	}
    
    @GetMapping("/deleteList")
	public String deleteList(Model model,@Param("email") String email){
        this.recipeRepository.deleteGroceryList(email);
		return "recipeHome";
	}

    @GetMapping("/sendList")
    public String sendList(Model model, @Param("email") String email) {
        String groceryList = this.recipeRepository.getCurrentList(email);
        String mobile = this.userRepository.getMobile(email);
        groceryList = groceryList.replaceAll(",", "\n");
        if (mobile != null) {
            
            try {
                TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
    
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("Body", groceryList));
                params.add(new BasicNameValuePair("To", mobile));
                params.add(new BasicNameValuePair("From", TWILIO_NUMBER));
    
                MessageFactory messageFactory = client.getAccount().getMessageFactory();
                Message message = messageFactory.create(params);
                System.out.println(message.getSid());
            } 
            catch (TwilioRestException e) {
                System.out.println(e.getErrorMessage());
            }
        }
        return "recipeHome";
    }

}
