package com.sweng894.whats4dinner.repository;

import java.util.List;

import com.sweng894.whats4dinner.entity.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    List<User> findByName(String name);

   // @Query(value = "SELECT u FROM User u WHERE u.email = :email", nativeQuery = true)
   @Query("SELECT u FROM User u WHERE u.email = ?1")
   User findByEmail(String email);

   @Query(value = "SELECT u.mobile FROM users u WHERE u.email= :email", nativeQuery = true)
   String getMobile(String email);

}
