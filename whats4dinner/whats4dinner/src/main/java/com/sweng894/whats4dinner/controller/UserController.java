package com.sweng894.whats4dinner.controller;

import java.util.List;

import com.sweng894.whats4dinner.entity.CustomUserDetails;
import com.sweng894.whats4dinner.entity.Recipe;
import com.sweng894.whats4dinner.entity.User;
import com.sweng894.whats4dinner.repository.RecipeRepository;
import com.sweng894.whats4dinner.repository.UserRepository;
import com.sweng894.whats4dinner.service.CustomUserDetailService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = {"/users"})
public class UserController {

    @Autowired
    private UserRepository userRepository;

	@Autowired RecipeRepository recipeRepository;

    @GetMapping("list")
	public String users (Model model) {
		model.addAttribute("users", this.userRepository.findAll());
		return "index";
	}

	@GetMapping("")
	public String showUser(Model model, @AuthenticationPrincipal CustomUserDetails userDetails ) {
		String email = userDetails.getUsername();
		User user = this.userRepository.findByEmail(email);
		model.addAttribute("email", email);
		model.addAttribute("user", user);
		 
		return "users";
	}

	@RequestMapping("/showRecipe")
	public String showSavedRecipes(Model model, @Param("recipe_id") Integer recipe_id ) {
		List <Recipe> recipes = this.recipeRepository.findByID(recipe_id);
		model.addAttribute("recipes", recipes);
		model.addAttribute("recipe_id", recipe_id);
		 
		return "users";
	}

}
