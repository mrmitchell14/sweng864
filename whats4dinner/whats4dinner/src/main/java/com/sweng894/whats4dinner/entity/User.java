package com.sweng894.whats4dinner.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.vladmihalcea.hibernate.type.array.IntArrayType;
import com.vladmihalcea.hibernate.type.array.StringArrayType;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;



@Entity
@Table(name = "users", schema = "public")
@TypeDefs({
    @TypeDef(
        name = "string-array",
        typeClass = StringArrayType.class
        )
})
public class User {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="id")
    @Column(name = "id")
	private long id;
	
    @Column(name = "name")
    private String name;

    @Column(name = "password")
	private String password;
	
    @Column(name = "email")
    private String email;

    @Column(name = "mobile")
    private String mobile;

   /* @Type(type = "int-array")
    @Column(
        name = "recipe_id",
        columnDefinition = "long[]"
    )*/
    @Column(name = "recipe_id")
    private Integer recipe_id;

    // @Type(type = "string-array")
    // @Column(
    //     name = "grocery_list",
    //     columnDefinition = "text[]"
    // )
    @Column(name = "grocery_list")
    private String grocery_list;

	public User() {
		super();
	}

    public User (long id, String name, String email, String password, String mobile, Integer recipe_id, String grocery_list ) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.mobile = mobile;
        this.recipe_id = recipe_id;
        this.grocery_list = grocery_list;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getRecipeID() {
        return recipe_id;
    }

    public void setRecipeID(Integer recipe_id) {
        this.recipe_id = recipe_id;
    }

    public String getGroceryList() {
        return grocery_list;
    }

    public void setGroceryList(String grocery_list) {
        this.grocery_list = grocery_list;
    }
}