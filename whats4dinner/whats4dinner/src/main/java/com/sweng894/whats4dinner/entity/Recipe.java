package com.sweng894.whats4dinner.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "recipes", schema = "public")
public class Recipe {
    
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="id")
    @Column(name = "id")
    private long id;
	
    @Column(name = "name")
    private String name; 

    @Column(name = "mainIngredient")
    private String mainIngredient;

    @Column(name = "sup_ingredients")
    private String sup_ingredients;

    @Column(name = "directions")
    private String directions;

    public Recipe() {
		super();
	}

    public Recipe (long id, String name, String mainIngredient, String sup_ingredients, String directions) {
        this.id = id;
        this.name = name;
        this.mainIngredient = mainIngredient;
        this.sup_ingredients = sup_ingredients;
        this.directions = directions;
    }

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMainIngredient() {
        return mainIngredient;
    }

    public void setMainIngredient(String mainIngredient) {
        this.mainIngredient = mainIngredient;
    }

    public String getSupIngredients() {
        return sup_ingredients;
    }

    public void setSupIngrenients(String sup_ingredients) {
        this.sup_ingredients = sup_ingredients;
    }

    public String getDirections() {
        return directions;
    }

    public void setDirections(String directions) {
        this.directions = directions;
    }
}
