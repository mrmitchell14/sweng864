package com.sweng894.whats4dinner.controller;

import com.sweng894.whats4dinner.entity.User;
import com.sweng894.whats4dinner.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = {"/"})
public class GeneralController {
    @Autowired
    UserRepository userRepository;

    @GetMapping("/createUser")
	public String showUserForm(User user) {
		return "add-user";
	}

    @PostMapping("createUser/add")
	public String addUser(@Validated User user, BindingResult result, Model model) {
		if(result.hasErrors()) {
			return "add-user";
		}
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String encodedPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);
		this.userRepository.save(user);
		return "user-created";
	}
}
