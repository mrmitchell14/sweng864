package com.sweng894.whats4dinner.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


import com.sweng894.whats4dinner.entity.Recipe;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe,Long> {
    
    @Query(value = "SELECT r.* FROM recipes r WHERE r.main_ingredient like %?1%", nativeQuery = true)
    List<Recipe> findByMainIngredient(String mainIngredient); 
    
    @Transactional
    @Modifying
    @Query(value = "UPDATE users u SET recipe_id = :recipe_id WHERE u.email = :email", nativeQuery = true)
    void saveRecipe(@Param("recipe_id")  Integer recipe_id, @Param("email") String email);
    
    @Query(value = "SELECT r.* FROM recipes r WHERE r.id = :recipe_id", nativeQuery = true)
    List<Recipe> findByID(Integer recipe_id); 

    @Transactional
    @Modifying
    @Query(value = "UPDATE users SET grocery_list = :newList WHERE email = :email", nativeQuery = true)
    void saveToList(@Param("newList") String newList, @Param("email") String email);

    @Query(value = "SELECT u.grocery_list FROM users u WHERE u.email= :email", nativeQuery = true)
    String getCurrentList(String email);
    
    @Transactional
    @Modifying
    @Query(value = "UPDATE users SET grocery_list = null WHERE email = :email", nativeQuery = true)
    void deleteGroceryList(String email);
}

