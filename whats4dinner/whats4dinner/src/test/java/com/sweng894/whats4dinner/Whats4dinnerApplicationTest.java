package com.sweng894.whats4dinner;

import com.sweng894.whats4dinner.controller.GeneralController;
import com.sweng894.whats4dinner.controller.RecipeController;
import com.sweng894.whats4dinner.controller.UserController;
import com.sweng894.whats4dinner.entity.Recipe;
import com.sweng894.whats4dinner.entity.User;
import com.sweng894.whats4dinner.repository.RecipeRepository;
import com.sweng894.whats4dinner.repository.UserRepository;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;


@SpringBootTest
@AutoConfigureMockMvc
class Whats4dinnerApplicationTest {

	@Autowired
	GeneralController generalController;

	@Autowired
	UserController userController;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RecipeController recipeController;
	
	@Autowired
	RecipeRepository recipeRepository;

	@Autowired
	private MockMvc mockMvc;

	@Before
	void deleteAllUser() {
		userRepository.deleteAll();

	}

	@Test
	void contextLoads() throws Exception {
		assertThat(userController).isNotNull();
	}

	@Test
	public void testHomePageStatus() throws Exception {
		this.mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void testCreateUserPageStatus() throws Exception {
		this.mockMvc.perform(get("/createUser")).andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void testUserAddPageStatus() throws Exception {
		User user = new User(1,"Test User","password1", "test1@email.com","",2,"");
		userRepository.save(user);
		User createdUser = userRepository.findByEmail(user.getEmail());
		assertThat( createdUser.getEmail() == "test@email.com");
		userRepository.deleteAll();
	}

	@Test
	public void testUserSaveRecipe() throws Exception {
		User user = new User(1,"Test User","password1", "test2@email.com","",2,"");
		userRepository.save(user);
		User createdUser = userRepository.findByEmail(user.getEmail());
		long rID = createdUser.getRecipeID();
		Recipe recipe = recipeRepository.getById(rID);
		assertThat( recipe.getId() == rID);
		userRepository.deleteAll();
	}

	@Test
	public void testHomePageTitle() throws Exception {
		this.mockMvc.perform(get("/")).andDo(print()).andExpect(content().string(containsString("What&#39;s For Dinner")));
	}

	@Test
	public void testRecipeHomePageStatus() throws Exception {
		this.mockMvc.perform(get("/recipes/home")).andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void testCreateGroceryList() throws Exception {
		User user = new User(2,"Test User","password1", "test3@email.com","",2,"");
		userRepository.save(user);
		String gList = "Cheese, milk, beans, rice";
		recipeRepository.saveToList(gList, user.getEmail());
		User createdUser = userRepository.findByEmail(user.getEmail());
		assertThat( createdUser.getGroceryList() == gList);
		userRepository.deleteAll();
	}

	@Test
	public void testDeleteGroceryList() throws Exception {
		User user = new User(7,"Test User","password1", "test5@email.com","",2,"Cheese, milk, beans, rice");
		userRepository.save(user);
		recipeRepository.deleteGroceryList(user.getEmail());
		User createdUser = userRepository.findByEmail(user.getEmail());
		assertThat( createdUser.getGroceryList() == null);
		userRepository.deleteAll();
	}
	
	@Test
	public void testViewGroceryList() throws Exception {
		User user = new User(1,"Test User","password1", "test6@email.com","",2,"Cheese, milk, beans, rice");
		userRepository.save(user);
		String currentList = recipeRepository.getCurrentList(user.getEmail());
		User createdUser = userRepository.findByEmail(user.getEmail());
		assertThat( createdUser.getGroceryList() ==  currentList);
		userRepository.deleteAll();
	}

	@Test
	public void testRecipeSearch() throws Exception {
		this.mockMvc.perform(get("/recipes/search?mainIngredient=noodles"))
		.andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void testGetRecipeByMainIngredient() throws Exception {
		String mainIngredient = "beans";
		Recipe recipe = new Recipe();
		mockMvc.perform(get("/recipes/search")
				.sessionAttr("recipe", recipe)
				.param("mainIngredient", mainIngredient)             
		).andDo(print())
		 .andExpect(status().isOk());
	}

	@Test
	public void testGetRecipeByMainIngredientNoResults() throws Exception {
		String mainIngredient = "fish";
		Recipe recipe = new Recipe();
		mockMvc.perform(get("/recipes/search")
				.sessionAttr("recipe", recipe)
				.param("mainIngredient", mainIngredient)             
		).andDo(print())
		 .andExpect(status().isOk());
	}

	@Test
	public void testUserNotAuthenticated () throws Exception {
		User user = new User();
		user.setEmail("daisy@email.com");
		user.setPassword("daisy");
		mockMvc.perform(post("/users").param("username", "daisy@email.com").param("password","daisy")).andDo(print()).andExpect(status().is(403));
        
	}

	@Test
	public void testSaveRecipe () throws Exception {
		User user = new User(1,"Test User","password1", "test6@email.com","",null,"Cheese, milk, beans, rice");
		userRepository.save(user);
		Integer rID = 3;
		recipeRepository.saveRecipe(rID, user.getEmail());
		User createdUser = userRepository.findByEmail(user.getEmail());
		assertThat( createdUser.getRecipeID() == 3);
		userRepository.deleteAll();

	}
}


